From 03699f0b49461b93b588cbb01b42ff1ded93c5ed Mon Sep 17 00:00:00 2001
From: Olivier FAURE <couteaubleu@gmail.com>
Date: Sun, 30 May 2021 12:01:49 +0200
Subject: [PATCH 1/5] Normalize formatting in audio_output/emscripten.cpp

---
 modules/audio_output/emscripten.cpp | 74 ++++++++++++++---------------
 1 file changed, 37 insertions(+), 37 deletions(-)

diff --git a/modules/audio_output/emscripten.cpp b/modules/audio_output/emscripten.cpp
index 1c890b3f6d..b704b90699 100644
--- a/modules/audio_output/emscripten.cpp
+++ b/modules/audio_output/emscripten.cpp
@@ -42,7 +42,7 @@
 #define AUDIO_WORKLET_NB_CHANNELS 2
 
 using namespace emscripten;
-namespace {	   
+namespace {
 	EM_BOOL requestAnimationFrame_cb( double time, void *userData );
 
 	class AWNodeWrapper {
@@ -50,31 +50,31 @@ namespace {
 		val context = val::undefined();
 		val getCtx() const { return context; };
 		void setCtx(val v_context) { context = v_context; };
-		
+
 		uintptr_t sab_ptr;
 		uintptr_t getSabPtr() const { return sab_ptr; };
 		void setSabPtr(uintptr_t p_sab) { sab_ptr = p_sab; };
-		
+
 		size_t sab_size;
 		size_t getSabSize() const { return sab_size; };
 		void setSabSize(size_t s_size) { sab_size = s_size; };
-		
+
 		int8_t channels;
 		int8_t getChannels() const { return channels; };
 		void setChannels(int8_t chan) { channels = chan; };
-		
+
 		AWNodeWrapper(int sample_rate) {
 			// Prepare audio context options
 			val audio_ctx_options = val::object();
 			audio_ctx_options.set("sampleRate", sample_rate);
-			
+
 			context = val::global("AudioContext").new_(audio_ctx_options);
 			context.call<void>("suspend");
 		}
-		
+
 		val operator()( val undefined_promise_argument ) {
 			(val)undefined_promise_argument;
-			
+
 			// Prepare AWN Options
 			val awn_options = val::object();
 			val awn_opt_outputChannelCount = val::array();
@@ -82,26 +82,26 @@ namespace {
 			awn_options.set("outputChannelCount", awn_opt_outputChannelCount);
 			awn_options.set("numberOfInputs", 0);
 			awn_options.set("numberOfOutputs", 1);
-			
+
 			val AudioNode = val::global("AudioWorkletNode").new_(context, std::string("worklet-processor"), awn_options);
 			AudioNode.set("channelCount", channels);
-			
+
 			//Prepare postMessage message
 			val msg = val::object();
 			msg.set("type", std::string("recv-audio-queue"));
 			msg.set("data", val::module_property("wasmMemory")["buffer"]);
 			msg.set("sab_ptr", sab_ptr);
 			msg.set("sab_size", sab_size);
-			
+
 			AudioNode["port"].call<val>("postMessage", msg);
 			AudioNode.call<val>("connect", context["destination"]);
-			
+
 			emscripten_request_animation_frame_loop(requestAnimationFrame_cb, this);
-			
+
 			return val::undefined();
 		}
 	};
-	
+
 	EMSCRIPTEN_BINDINGS(AWWSCOPE) {
 		class_<AWNodeWrapper>("awn_cb_wrapper")
 			.constructor<int>()
@@ -111,14 +111,14 @@ namespace {
 			.property("channels", &AWNodeWrapper::getChannels, &AWNodeWrapper::setChannels)
 			.function("awn_call", &AWNodeWrapper::operator());
 	};
-	
+
 	typedef struct aout_sys_t
 	{
 		int8_t *sab;
 		size_t sab_size;
 		AWNodeWrapper *awn_inst;
 		float volume;
-		
+
 	} aout_sys_t;
 
 	EM_BOOL requestAnimationFrame_cb( double time, void *userData ) {
@@ -134,26 +134,26 @@ namespace {
 		}
 		return EM_TRUE;
 	}
-	
+
 	// For Atomics.store() and .load() only integer types are supported
 	unsigned int js_index_load(int8_t *sab_ptr, int8_t index, size_t sab_size){
-		uint32_t *buffer_view = reinterpret_cast<uint32_t *>(sab_ptr); 
+		uint32_t *buffer_view = reinterpret_cast<uint32_t *>(sab_ptr);
 		val buffer = val(typed_memory_view(sab_size, buffer_view));
-		
+
 		return val::global("Atomics").call<unsigned int>("load", buffer, index);
 	}
-	
+
 	void js_index_store(int8_t *sab_ptr, int8_t index, unsigned int value, size_t sab_size) {
-		uint32_t *buffer_view = reinterpret_cast<uint32_t *>(sab_ptr); 
+		uint32_t *buffer_view = reinterpret_cast<uint32_t *>(sab_ptr);
 		val buffer = val(typed_memory_view(sab_size, buffer_view));
-		
+
 		return val::global("Atomics").call<void>("store", buffer, index, value);
 	}
 
 	// careful when calling this, you cannot wait on any index
 	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Atomics/wait
 	unsigned int js_index_wait(int8_t *sab_ptr, int8_t index, size_t sab_size) {
-		int32_t *buffer_view = reinterpret_cast<int32_t *>(sab_ptr); 
+		int32_t *buffer_view = reinterpret_cast<int32_t *>(sab_ptr);
 		val buffer = val(typed_memory_view(sab_size, buffer_view));
 
 		return val::global("Atomics").call<unsigned int>("wait", buffer, index);
@@ -164,7 +164,7 @@ namespace {
 		aout_sys_t * sys = reinterpret_cast<aout_sys_t *>(aout->sys);
 		bzero(sys->sab, sys->sab_size);
 	}
-	
+
 	int Start( audio_output_t *aout, audio_sample_format_t *restrict fmt )
 	{
 		aout_sys_t *sys = reinterpret_cast<aout_sys_t *>(aout->sys);
@@ -187,7 +187,7 @@ namespace {
 	{
 		Flush(aout);
 	}
-	
+
 	int audio_worklet_push (audio_output_t *aout, const int8_t *data, unsigned data_size) {
 		aout_sys_t *sys = reinterpret_cast<aout_sys_t *>(aout->sys);
 		int8_t *sab_view = sys->sab + 5 * sizeof(int32_t);
@@ -199,7 +199,7 @@ namespace {
 			unsigned data_size_copy_end = STORAGE_SIZE - head;
 			memcpy(sab_view + head, data, data_size_copy_end);
 			head = 0;
-			
+
 			// Copy the part of the data at the buffer start
 			unsigned data_size_copy_start = data_size - data_size_copy_end;
 			memcpy(sab_view + head, data, data_size_copy_start);
@@ -219,7 +219,7 @@ namespace {
 		aout_sys_t *sys = reinterpret_cast<aout_sys_t *>(aout->sys);
 		const int8_t* data = (int8_t *)block->p_buffer;
 		size_t data_size = block->i_buffer;
-		
+
 		unsigned head = js_index_load(sys->sab, 1, sys->sab_size);
 		unsigned tail = js_index_load(sys->sab, 2, sys->sab_size);
 		unsigned new_head = (head + data_size) % STORAGE_SIZE;
@@ -232,7 +232,7 @@ namespace {
 		audio_worklet_push(aout, data, data_size);
 		block_Release(block);
 	}
-	
+
 	void Pause( audio_output_t *aout, bool paused, vlc_tick_t date )
 	{
 		aout_sys_t * sys = reinterpret_cast<aout_sys_t *>(aout->sys);
@@ -245,7 +245,7 @@ namespace {
 		VLC_UNUSED(date);
 		Flush(aout);
 	}
-	
+
 	int Time_Get( audio_output_t *aout, vlc_tick_t *delay)
 	{
 		return aout_TimeGetDefault(aout, delay);
@@ -289,16 +289,16 @@ namespace {
 		return 0;
 	}
 
-	
+
 	int Open( vlc_object_t *obj )
 	{
 		audio_output_t * aout = (audio_output_t *) obj;
-		
+
 		/* Allocate structures */
 		aout_sys_t *sys = reinterpret_cast<aout_sys_t *>(malloc( sizeof( *sys ) ));
 		if( unlikely(sys == NULL) )
 			return VLC_ENOMEM;
-		
+
 		aout->sys = sys;
 		aout->start = Start;
 		aout->stop = Stop;
@@ -308,7 +308,7 @@ namespace {
 		aout->time_get = Time_Get;
 		aout->volume_set = Volume_Set;
 		aout->mute_set = Mute_Set;
-		
+
 		sys->awn_inst = new AWNodeWrapper(AUDIO_WORKLET_SAMPLE_RATE);
 		sys->sab_size = 5 * sizeof(int32_t) + STORAGE_SIZE;
 		sys->sab = reinterpret_cast<int8_t *>(malloc( sys->sab_size ));
@@ -317,9 +317,9 @@ namespace {
 		if ( unlikely(sys->sab == NULL) )
 			return VLC_ENOMEM;
 		bzero(sys->sab, sys->sab_size);
-		
+
 		val webaudio_context = sys->awn_inst->getCtx();
-		
+
 		// Prepare audioWorkletProcessor blob
 		val document = val::global("document");
 		val script = document.call<val>("createElement", std::string("SCRIPT"));
@@ -376,7 +376,7 @@ registerProcessor('worklet-processor', Processor);";
 		val BlobObject = val::object();
 		BlobObject.set("type", std::string("application/javascript"));
 		val WorkletModuleUrl = val::global("URL").call<val>("createObjectURL", val::global("Blob").new_(ProcessorTextArray, BlobObject));
-		
+
 		// Prepare audioWorkletProcessor callback
 		val cb_caller = val::module_property("awn_cb_wrapper").new_(AUDIO_WORKLET_SAMPLE_RATE);
 		cb_caller.set("context", val(webaudio_context));
@@ -385,7 +385,7 @@ registerProcessor('worklet-processor', Processor);";
 		cb_caller.set("channels", val(AUDIO_WORKLET_NB_CHANNELS));
 		val awn_caller = cb_caller["awn_call"];
 		val awn_cb = awn_caller.call<val>("bind", cb_caller);
-		
+
 		// start audio worklet (since the context is suspended, sound won't start now
 		// Since the WebAudio Context cannot be created in a worker, we create
 		// it in the main_thread and use the SAB to signal it when we want it to start
-- 
2.32.0

